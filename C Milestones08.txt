Modify the sort you have implemented to be able to sort any data type

int max = 20;
#include<stdio.h>
int scan_check()
{int a;

	while(scanf("%d",&a) == 0)
	{
			printf("Wrong input please try again.\n");
			getchar();
	}

	return a;

}

int str_length(char *str)
{	
	int i = 0;
	while(str[i++] != '\0');

	return i - 1;
}

int choice()
{
	int ch;
	printf("Enter which type of sort you want to do.\n1.Ascending sort.\t2.Descending sort\n");	
	do
	{
		ch = scan_check();	
	}while(ch != 1 && ch != 2);
	return ch&1;
}

int sortf(float *a,int n)
{
	int i,j,ch,temp;
	ch = choice();
	for(j = 0;j < n-1;j++)
	{
			for(i = 0;i < n-1-j;i++)
			{
				if(a[i + ch] < a[i+1 - ch])
				{
					temp = a[i];
					a[i] = a[i+1];
					a[i+1] = temp;
				}
			}
	}
	
	for(i = 0;i < n;i++)
	{
		printf("%f\t",a[i]);
	}
	
	return 0;
}

int sorti(int *a,int n)
{
	int i,j,ch,temp;
	ch = choice();
	for(j = 0;j < n-1;j++)
	{
			for(i = 0;i < n-1-j;i++)
			{
				if(a[i + ch] < a[i+1 - ch])
				{
					temp = a[i];
					a[i] = a[i+1];
					a[i+1] = temp;
				}
			}
	}
	
	for(i = 0;i < n;i++)
	{
		printf("%d\t",a[i]);
	}
	
	return 0;
}

char* sort(char *a)
{
	int i,j,ch,temp,n;
	ch = choice();
	n = str_length(a);
	for(j = 0;j < n-1;j++)
	{
			for(i = 0;i < n-1-j;i++)
			{
				if(a[i + ch] < a[i+1 - ch])
				{
					temp = a[i];
					a[i] = a[i+1];
					a[i+1] = temp;
				}
			}
	}
	return a;
}

int main()
{	
	int ai[max];
	char ac[max];
	float af[max];
	int ch,n,j;

	printf("Enter which data type you want.\nPress 1.Integer\t2.Float\t3.Char\n");
	
	do
	{
		ch = scan_check();	
	}while(ch != 1 && ch != 2 && ch != 3);	
	
	switch(ch)
	{
		case 1:printf("Enter how many integer you want to enter.\n");

			do
			{
				n = scan_check();

			}while(n < 0 && n > max);
			printf("Enter the integer element.\n");
			for(j = 0;j < n;j++)
			{		
				if(!scanf("%d",&ai[j]))
					getchar();
			}
			sorti(ai,n);
			break;

		case 2: printf("Enter how many float you want to enter.\n");
			do
			{
				n = scan_check();
			}while(n < 0 && n > max);	
			printf("Enter the float element.\n");
			for(j = 0;j < n;j++)
			{
				if(!scanf("%f",&af[j]))
					getchar();
			}
			sortf(af,n);
			break;

		case 3:printf("Enter the character array.\n");
				scanf(" %s",ac);
				printf("%s",sort(ac));
			break;
	}	

	
	return 0;
}

