A) Program to convert weight entered in pounds(lbs) to kilogram(kg)
Code-
#include<stdio.h>
int main()
{
float pounds,kg,ptokg=0.453592;
printf("ENTER WEIGHT VALUE IN POUNDS = ");
scanf("%f",&pounds);
kg=ptokg*pounds;
printf("THE CONVERSION OF WEIGHT FROM POUNDS TO KILOGRAM IS = %f",kg);
return 0;
Output-
ENTER WEIGHT VALUE IN POUNDS = 65
THE CONVERSION OF WEIGHT FROM POUNDS TO KILOGRAM IS = 29.483480

B) Program to convert length entered in inches to centimeters(cm)
Code-
#include<stdio.h>
int main()
{
float inch,cm,itoc=2.54;
printf("ENTER LENGTH VALUE IN INCH = ");
scanf("%f",&inch);
cm=itoc*inch;
printf("THE CONVERSION OF LENGTH FROM INCH TO CENTIMETER IS = %f",cm);
return 0;
}
Output-
ENTER LENGTH VALUE IN INCH = 30
THE CONVERSION OF LENGTH FROM INCH TO CENTIMETER IS = 76.199997

C) Program to convert temperature in Farenheit(F) to Celcius(C)
Code-
#include<stdio.h>
int main()
{
float fl,c;
printf("ENTER TEMP VALUE IN FERENHEIT = ");
scanf("%f",&fl);
c=((fl-32)*5/9);
printf("THE CONVERSION OF TEMP FROM FERENHEIT TO CELCIUS  
IS = %f",c);
return 0;
}
Output-
ENTER TEMP VALUE IN FERENHEIT = 265
THE CONVERSION OF TEMP FROM FERENHEIT TO CELCIUS IS = 129.444443

D) Program to calculate the Area of a Circle
Code-
#include<stdio.h>
int main()
{
float r,a;
printf("ENTER RADIUS OF A CIRCLE = ");
scanf("%f",&r);
a=r*r*3.14;
printf("THE AREA OF A CIRCLE IS = %f",a);
return 0;
}
Output-
ENTER RADIUS OF A CIRCLE = 10
THE AREA OF A CIRCLE IS = 314.000000

E) Program to calculate the Area of a Rectangle
Code-
#include<stdio.h>
int main()
{
float l,b;
printf("ENTER LENGTH OF A RECTANGLE = ");
scanf("%f",&l);
printf("ENTER BREADTH OF A RECTANGLE = ");
scanf("%f",&b);
printf("THE AREA OF A RECTANGLE IS = %f",l*b);
return 0;
}
Output-
ENTER LENGTH OF A RECTANGLE = 15
ENTER BREADTH OF A RECTANGLE =10
THE AREA OF A RECTANGLE IS =150.00000

F) Program to calculate the Area of a Triangle
Code-
#include<stdio.h>
int main()
{
float b,h;
printf("ENTER BASE OF A TRIANGLE = ");
scanf("%f",&b);
printf("ENTER HEIGHT OF A TRIANGLE = ");
scanf("%f",&h);
printf("THE AREA OF A TRIANGLE IS = %f",b*h*0.5);
return 0;
}
Output-
ENTER BASE OF A TRIANGLE = 12
ENTER HEIGHT OF A TRIANGLE = 15
THE AREA OF A TRIANGLE IS =90.000000

