a.int str_length(char *str) - returns the length of the string or -1 on error

#include<stdio.h>
#include<conio.h>

int str_length(char *str);
int str_copy(char *str1, char *str2); 

void main(){
	char a[200], b[100];
	int length;
	
	
	printf("enter string one: ");
	scanf(" %s",a);
	printf("enter string two: ");
	scanf(" %s",b);
	length = str_copy(a,b);
	printf("%d",length);
	
}

int str_copy(char *str1, char *str2){
	str1[0]='\0';
	int i = 0;
	while(str2[i]!='\0'){
		str1[i]=str2[i];
		i++;
	}
	str1[i]='\0';
	while(i==0){
		if(str1[i] != str2[i])
			return -1;
		i--;
	}
	return 0;

 }



int str_length(char *str){
	
	int i = 0;
	while(str[i]!='\0'){
		i++;
	}
	
	
	
	if(i==0)
		return -1;
	return i;
	
}

c. int str_compare(char *str1, char *str2) - if str1 is less than str2 the function should return -1 if str1 is greater than str2 it should return 1 and if both are equal then return 0

#include<stdio.h>
#include<conio.h>

int str_compare(char *str1, char *str2);

void main(){
	
	int ans;
	char a[200], b[200];
	printf("enter first string : ");
	scanf("%s", a);
	printf("enter second string : ");
	scanf("%s",b);
	ans = str_compare(a,b);
	printf("%d",ans);
	
}

int str_compare(char *str1, char *str2){
	
	int i = 0;
	
	while(str1[i] != '\0' || str2[i] != '\0'){
		
		if(str1[i] > str2[i])
			return 1;
		else if(str1[i] < str2[i])
			return -1;
		else 
			i++;
		
	}
	return 0;
	
}


d.int str_find_char(char *str, char *ch) - returns the position where ch is in str and if not present returns -1

#include<stdio.h>
#include<conio.h>

int str_find_char(char *str, char *ch);

void main() {
	
	char a[200];
	int position;
	char b;
	
	printf("Enter a string : ");
	scanf("%s",a);
	printf("Enter a character : ");
	scanf(" %c", &b);
	position = str_find_char(a, b);
	
	printf("%d",position+1);
	
}

int str_find_char(char *str, char *ch){
	
	int i = 0;
	while(str[0] != '\0') {
		
		if(str[i] == ch)
			return i;
		i++;
	}
	return -1;	
}

